Printer Cups Module
###################

The printer module allows administrators to define rules to decide if reports
are sent to the client, sent to a specific printer or dropped.

The module connects to a local CUPS [1] server and obtains all available
printers creating them as a 'printer' record.

This module needs an adequately patched server in order to function.

[1] http://www.cups.org
